﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DownloadTerremoto
{
    public class Program
    {
        public static string rootDir = @"C:\Git\";
        public static string projectDir = "";
        public static string pathOfLocal = @"C:\buildsrv3local\releases\Adgistics.Terremoto\archive";
        public static string pathOfServer = @"L:\releases\Adgistics.Terremoto\archive";
        public static string environment = "";
        public static string version = "";

        public static string Project
        {
            get { return projectDir.Split('\\').Last(); }
        }

        public static void DeleteBinAndObjFiles()
        {
            var filesToDelete = Directory.EnumerateFiles(projectDir + @"\src\", "*.*", SearchOption.AllDirectories)
                .Where(file => file.Contains("\\bin\\") || file.Contains("\\obj\\") );
            Console.WriteLine("Clearing bin and obj files...");
            foreach (var f in filesToDelete)
                File.Delete(f);
            Console.WriteLine("bin and obj files cleared.");
        }

        public static void Main(string[] args)
        {
            ChooseProject();
            Console.Clear();
            string filePath;
            try
            {
                filePath = Directory.EnumerateFiles(projectDir + @"\src\asp.net\", "*.csproj", SearchOption.AllDirectories).First();
            }
            catch (DirectoryNotFoundException)
            {
                Console.WriteLine("This is not a brand hub project.");
                CountDownToClose(10);
                return;
            }
            using (var reader = new StreamReader(filePath))
            {
                var versionRegex = new Regex(@"[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+");
                var prodOrBeta = new Regex(@"(production)|(beta)");
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (versionRegex.IsMatch(line) && prodOrBeta.IsMatch(line))
                    {
                        environment = prodOrBeta.Match(line).Value;
                        version = versionRegex.Match(line).Value;
                        break;
                    }
                }
            }
            DeleteBinAndObjFiles();
            Console.WriteLine("The current version for " + Project + " is " + version + ".");
            var pathToLookIn = pathOfLocal + @"\" + environment;
            Console.Out.WriteLine("Looking for " + version + " in " + pathToLookIn + "...");
            var isSuchVersion = Directory.Exists(pathToLookIn + "\\" + version);
            Console.Out.WriteLine(version + (isSuchVersion ? " is already downloaded." : " is not downloaded."));
            if (!isSuchVersion)
            {
                Task t = Task.Run(() =>
               {

                   Microsoft.VisualBasic.FileIO.FileSystem.CopyDirectory(pathOfServer + "\\" + environment + "\\" + version
                       , pathOfLocal + "\\" + environment + "\\" + version);
                   Console.Out.WriteLine();
                   Console.Out.WriteLine("Copied successfully.");
               });
                var dots = 3;
                while(!t.IsCompleted)
                {
                    dots = DownloadBar(dots);
                }
                t.Wait();
            }
            CountDownToClose(10);
        }

        public static void ChooseProject()
        {
            var files = Directory.GetDirectories(rootDir);
            Console.WriteLine("Choose Project By Number");
            for (var i = 0; i < files.Count(); i++)
            {
                Console.WriteLine(i + ". " + files[i].Split('\\').Last());
            }
            var result = Console.ReadLine();
            int fileIndex;
            while (!int.TryParse(result, out fileIndex) || fileIndex < 0 || fileIndex >= files.Count())
            {
                Console.WriteLine("Invalid choice. Please choose a number between 0 and {0}.", files.Count() - 1);
                result = Console.ReadLine();
            }
            projectDir = files[fileIndex];
        }

        public static void CountDownToClose(int seconds)
        {
            for (var i = seconds; i >= 0; i--)
            {
                Console.Write("\rProgram will close in {0} seconds...", i);
                System.Threading.Thread.Sleep(1000);
            }
        }

        public static int DownloadBar(int dots)
        {
            var periods = "";
            for (int i = dots; i >= 0; i--)
            {
                periods += ".";
            }
            Console.Write("\r" + new string(' ', Console.WindowWidth - 1) + "\r");
            Console.Write("\rDownloading" + periods);
            System.Threading.Thread.Sleep(500);
            if (dots == 0)
                return 3;
            else
                return --dots;
        }
    }
}
